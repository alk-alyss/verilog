module shift_register_tb;
parameter n = 4;

reg clk, s_in, ctrl, rst;
reg [n-1:0] d;
output out;

initial begin
	$dumpfile("shift_register_tb.vcd");
	$dumpvars(0, shift_register_tb);
	$monitor("time=%2t clk=%b ctrl=%b rst=%b s_in=%b d=%b out=%b", $time, clk, ctrl, rst, s_in, d, out);

	clk = 1'b0;
	rst = 1'b1;
	ctrl = 1'b1;
	s_in = 1'b0;
	d = 4'b1101;

	#10 rst = 1'b0;
	#10 ctrl = 1'b0;

	#10 s_in = 1'b1;
	#10 s_in = 1'b0;
	#10 s_in = 1'b0;
	#10 s_in = 1'b1;
	#10 s_in = 1'b0;

	#40 ctrl = 1'b1;
	d = 4'b1010;
	#10 ctrl = 1'b0;
	

	#40 $finish;

end

always begin
	#5 clk = ~clk;
end

shift_register #(.n(n)) CUT1 (
	.clk(clk),
	.s_in(s_in),
	.d(d),
	.ctrl(ctrl),
	.rst(rst),
	.out(out)
);

endmodule
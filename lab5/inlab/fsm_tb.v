module fsm_tb;

parameter CLOCK = 5;

reg clk, reset, a, b;
output z;

initial begin
	$dumpfile("fsm_tb.vcd");
	$dumpvars(0, fsm_tb);
	$monitor("time=%2t clk=%b reset=%b a=%b b=%b z=%b", $time, clk, reset, a, b, z);

	clk = 1'b0;
	reset = 1'b0;
	a = 1'b0;
	b = 1'b0;

	#10 a = 1'b1;
	#10 reset = 1'b1;
	#10 reset = 1'b0;
	#30 a = 1'b0;

	#10 b = 1'b1;
	#20 b = 1'b0;

	#10 b = 1'b1;
	#10 b = 1'b0; a = 1'b1;
	#10 a = 1'b0;

	#10 a = 1'b1;
	#10 a = 1'b0; b = 1'b1;
	#10 b = 1'b0;

	#10 a = 1'b1;
	#20 a = 1'b0; b = 1'b1;
	#10 b = 1'b0;

	#10 $finish;
end

always begin
	#CLOCK clk = ~clk;
end

fsm CUT1(
	.a(a),
	.b(b),
	.clk(clk),
	.reset(reset),
	.z(z)
);

endmodule
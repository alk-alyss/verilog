module cla4(cin, a, b, s, cout);

input cin;
input [3:0] a, b;
output reg [3:0] s;
output reg cout;

reg [3:0] p, g;
reg [4:0] c;

always @* begin
	c[0] = cin;

	for(integer i=0; i<4; i=i+1) begin
		g[i] = a[i]*b[i];
		p[i] = a[i]^b[i];
		s[i] = p[i]^c[i];
		c[i+1] = g[i]+p[i]*c[i];
	end

	cout = c[4];
end

endmodule
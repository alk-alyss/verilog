module cla4_tb;

reg cin;
reg [3:0] a, b;
output cout;
output [3:0] s;

initial begin
	$dumpfile("cla4_tb.vcd");
	$dumpvars(0, cla4_tb);
	$monitor("time=%2t cin=%b a=%b b=%b s=%b cout=%b", $time, cin, a, b, s, cout);

	cin = 1'b0; a = 4'hF; b = 4'h1;
	#5 cin = 1'b0; a = 4'hF; b = 4'h0;
	#5 cin = 1'b0; a = 4'h5; b = 4'h1;
	#5 cin = 1'b0; a = 4'h1; b = 4'h1;
	#5 cin = 1'b0; a = 4'hA; b = 4'h5;
	#5 cin = 1'b1; a = 4'hA; b = 4'h5;
	#5 cin = 1'b0; a = 4'hD; b = 4'h0;
	#5 cin = 1'b1; a = 4'h1; b = 4'h1;

	#5 $finish;
end

cla4 CUT1 (.cin(cin), .a(a), .b(b), .s(s), .cout(cout));

endmodule

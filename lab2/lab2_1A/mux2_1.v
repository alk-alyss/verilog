module mux2_1(a, b, s, y);

input a, b, s;
output reg y;

always @*
	case (s)
		1'b0: y = b;
		1'b1: y = a;
	endcase

endmodule
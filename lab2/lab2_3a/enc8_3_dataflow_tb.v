module enc8_3_dataflow_tb;

reg [7:0] d;
output [2:0] y;

initial	begin
	$dumpfile("enc8_3_dataflow_tb.vcd");
	$dumpvars(0, enc8_3_dataflow_tb);
	$monitor("time=%2t d=%b y=%b", $time, d, y);

	d = 1'h0;

	for(integer i=0; i<8; i=i+1) begin
		#5 d = 0;
		d[i] = 1;
	end

	#5 $finish;
end

enc8_3_dataflow cut1 (.d(d), .y(y));

endmodule

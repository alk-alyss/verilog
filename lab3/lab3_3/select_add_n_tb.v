module select_add_n_tb;
parameter n = 12;

reg [n-1:0] a, b;
reg cin;
output [n-1:0] s;
output cout;

initial begin
	$dumpfile("select_add_n_tb.vcd");
	$dumpvars(0, select_add_n_tb);
	$monitor("time=%2t cin=%b a=%b b=%b s=%b cout=%b", $time, cin, a, b, s, cout);

	cin = 1'b0; a = 12'hFFF; b = 12'h001;
	#5 cin = 1'b0; a = 12'hFFF; b = 12'h000;
	#5 cin = 1'b0; a = 12'h405; b = 12'h401;
	#5 cin = 1'b0; a = 12'h801; b = 12'h821;
	#5 cin = 1'b0; a = 12'hAAA; b = 12'h555;
	#5 cin = 1'b1; a = 12'hAAA; b = 12'h555;
	#5 cin = 1'b0; a = 12'h7FD; b = 12'h000;
	#5 cin = 1'b1; a = 12'h001; b = 12'h001;
	
	#5 $finish;
end

select_add_n #(.n(n)) CUT1 (
	.cin(cin),
	.a(a),
	.b(b),
	.s(s),
	.cout(cout)
);

endmodule
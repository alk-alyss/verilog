module shift_register (clk, in, rst, out);
parameter n = 3;

input clk, in, rst;
output out;

wire [n:0] d;

assign d[0] = in;
genvar i;

generate
	for(i=0; i<n; i=i+1) begin
		d_ff D1 (
			.clk(clk),
			.d(d[i]),
			.rst(rst),
			.q(d[i+1])
		);
	end
endgenerate

assign out = d[n];

endmodule
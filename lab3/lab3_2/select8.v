module select8(cin, a, b, s, cout);

input cin;
input [7:0] a, b;
output [7:0] s;
output cout;

wire [3:0] c, r, t;

assign c[0] = cin;

cla4 U1 (
	.cin(c[0]), 
	.a(a[3:0]), 
	.b(b[3:0]), 
	.s(s[3:0]), 
	.cout(c[1])
);

cla4 U2 (
	.cin(1'b0), 
	.a(a[7:4]), 
	.b(b[7:4]), 
	.s(t), 
	.cout(c[2])
);

cla4 U3 (
	.cin(1'b1), 
	.a(a[7:4]), 
	.b(b[7:4]), 
	.s(r), 
	.cout(c[3])
);

muxN2_1 #(.n(4))M1 (.a(t), .b(r), .s(c[1]), .y(s[7:4]));
mux2_1 M2 (.a(c[2]), .b(c[3]), .s(c[1]), .y(cout));

endmodule
module shift_register_tb;
parameter n = 4;

reg clk, in, rst;
output out;

initial begin
	$dumpfile("shift_register_tb.vcd");
	$dumpvars(0, shift_register_tb);
	$monitor("time=%2t clk=%b rst=%b in=%b out=%b", $time, clk, rst, in, out);

	clk = 1'b0;
	rst = 1'b1;
	in = 1'b0;

	#10 rst = 1'b0;

	#10 in = 1'b1;
	#10 in = 1'b0;
	#10 in = 1'b0;
	#10 in = 1'b1;
	#10 in = 1'b0;

	#40 $finish;

end

always begin
	#5 clk = ~clk;
end

shift_register #(.n(n)) CUT1 (
	.clk(clk),
	.in(in),
	.rst(rst),
	.out(out)
);

endmodule
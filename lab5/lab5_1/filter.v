module filter(in, clk, reset, out);

input in, clk, reset;
output reg out;

parameter SIZE = 2;
parameter s0=2'b00,s1=2'b01,s2=2'b10,s3=2'b11;

reg [SIZE-1:0] state, next_state;

always @ (posedge clk)
begin : FSM_SEQUENTIAL_LOGIC
	if (reset == 1'b1)
		state <= s0;
	else
		state <= next_state;
end

always @ reset
begin : ASYNCHRONOUS_RESET
	if (reset == 1'b1)
		state = s0;
end

always @ (state, in)
begin : FSM_COMBINATIONAL_INPUT_LOGIC
	next_state = 2'b00;
	case (state)
	s0: if(in == 1'b1) begin
			next_state = s1;
		end else begin
			next_state = s0;
		end
	s1: if(in == 1'b1) begin
			next_state = s2;
		end else begin
			next_state = s0;
		end
	s2: if(in == 1'b1) begin
			next_state = s2;
		end else begin
			next_state = s3;
		end
	s3: if(in == 1'b1) begin
			next_state = s2;
		end else begin
			next_state = s0;
		end
	default: next_state = s0;
	endcase
end

always @ (state, reset)
begin : MOORE_OUTPUT_LOGIC
	if(reset == 1'b1)
		out = 1'b0;
	else begin
		case (state)
		s0: out = 1'b0;
		s1: out = 1'b0;
		s2: out = 1'b1;
		s3: out = 1'b1;
		default: out = 1'b0;
		endcase
	end
end

endmodule
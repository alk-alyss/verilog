module mux2_1 (a, b, s, y);

input a, b, s;
output y;

assign y = s == 1'b1 ? b : a;

endmodule
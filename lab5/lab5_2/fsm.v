module fsm(in, clk, reset, out);

input in, clk, reset;
output reg out;

parameter SIZE = 3;
parameter s0=3'b000,s1=3'b001,s2=3'b010,s3=3'b011,s4=3'b100,s5=3'b101,s6=3'b110,s7=3'b111;

reg [SIZE-1:0] state, next_state;

always @ (posedge clk)
begin : FSM_SEQUENTIAL_LOGIC
	if (reset == 1'b1)
		state <= s0;
	else
		state <= next_state;
end

always @ (state, in)
begin : FSM_COMBINATIONAL_INPUT_LOGIC
	next_state = 2'b00;
	case (state)
	s0: if(in == 1'b1) begin
			next_state = s1;
		end else begin
			next_state = s0;
		end
	s1: if(in == 1'b1) begin
			next_state = s2;
		end else begin
			next_state = s0;
		end
	s2: if(in == 1'b1) begin
			next_state = s3;
		end else begin
			next_state = s0;
		end
	s3: if(in == 1'b0) begin
			next_state = s4;
		end else begin
			next_state = s0;
		end
	s4: if(in == 1'b1) begin
			next_state = s5;
		end else begin
			next_state = s0;
		end
	s5: if(in == 1'b0) begin
			next_state = s6;
		end else begin
			next_state = s0;
		end
	s6: if(in == 1'b1) begin
			next_state = s1;
		end else begin
			next_state = s0;
		end
	default: next_state = s0;
	endcase
end

always @ (state, reset)
begin : MOORE_OUTPUT_LOGIC
	if(reset == 1'b1)
		out = 1'b0;
	else begin
		case (state)
		s6: out = 1'b1;
		default: out = 1'b0;
		endcase
	end
end

endmodule
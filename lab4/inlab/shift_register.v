module shift_register (clk, s_in, d, ctrl, rst, out);
parameter n = 3;

input [n-1:0] d;
input clk, s_in, ctrl, rst;
output out;

wire [n-1:0] din;
wire [n:0] dout;

assign dout[0] = s_in;

genvar i;

generate
	for(i=0; i<n; i=i+1) begin
		mux2_1 M1 (
			.a(dout[i]),
			.b(d[i]),
			.s(ctrl),
			.y(din[i])
		);
		d_ff D1 (
			.clk(clk),
			.d(din[i]),
			.rst(rst),
			.q(dout[i+1])
		);
	end
endgenerate

assign out = dout[n];

endmodule
module select_add_n(cin, a, b, s, cout);
parameter n = 8;

input cin;
input [n-1:0] a, b;
output [n-1:0] s;
output cout;

wire [n/4:0] c;

assign c[0] = cin;

cla4 U1 (
	.cin(c[0]),
	.a(a[3:0]),
	.b(b[3:0]),
	.s(s[3:0]),
	.cout(c[1])
);

genvar i;
generate
	for(i=1; i<n/4; i=i+1) begin
		inner_module U (
			.cin(c[i]),
			.a(a[i*4+3:i*4]),
			.b(b[i*4+3:i*4]),
			.s(s[i*4+3:i*4]),
			.cout(c[i+1])
		);
	end
endgenerate

assign cout = c[n/4];

endmodule
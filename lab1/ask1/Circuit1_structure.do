onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -height 40 -label A /Circuit1_structure_testbench/A
add wave -noupdate -height 40 -label B /Circuit1_structure_testbench/B
add wave -noupdate -height 40 -label C /Circuit1_structure_testbench/C
add wave -noupdate -height 40 -label D /Circuit1_structure_testbench/D
add wave -noupdate -height 40 -label F /Circuit1_structure_testbench/F
add wave -noupdate -height 40 -label w1 /Circuit1_structure_testbench/CUT1/w1
add wave -noupdate -height 40 -label w2 /Circuit1_structure_testbench/CUT1/w2
add wave -noupdate -height 40 -label w3 /Circuit1_structure_testbench/CUT1/w3
add wave -noupdate -height 40 -label w4 /Circuit1_structure_testbench/CUT1/w4
add wave -noupdate -height 40 -label w5 /Circuit1_structure_testbench/CUT1/w5
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 73
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {84 ns}

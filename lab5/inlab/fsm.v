module fsm(a, b, reset, clk, z);

input a, b, reset, clk;
output reg z;

parameter SIZE=2;
parameter S0=2'b00, S1=2'b01, S2=2'b10, S3=2'b11;

reg [SIZE-1:0] state, next_state;

always @ (posedge clk) 
begin : FSM_SEQUENTIAL_LOGIC
	if(reset == 1'b1)
		state <= S0;
	else
		state <= next_state;
end

always @ (state, a, b)
begin : FSM_COMBINATIONAL_INPUT_LOGIC
	next_state = 2'b00;
	case (state)
	S0: if (a == 1'b1) begin
			next_state = S1;
		end else if (b == 1'b1) begin
			next_state = S2;
		end else begin
			next_state = S0;
		end
	S1: if (a == 1'b1) begin
			next_state = S2;
		end else if (b == 1'b1) begin
			next_state = S3;
		end else begin
			next_state = S1;
		end
	S2: if (a == 1'b1 || b == 1'b1) begin
			next_state = S3;
		end else begin
			next_state = S2;
		end
	S3: begin
			z = 1'b1;
			next_state = S0;
		end
	default: next_state = S0;
	endcase
end

always @ (state, reset)
begin : MOORE_OUTPUT_LOGIC
	if(reset == 1'b1)
		z = 1'b0;
	else begin
	case (state)
		S0: z = 1'b0;
		S1: z = 1'b0;
		S2: z = 1'b0;
		S3: z = 1'b1;
		default: z = 1'b0;
	endcase
	end
end
				

endmodule
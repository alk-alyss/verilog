module muxN_1_tb_a;
parameter n = 32;

reg [n-1:0] a, b, s;
output [n-1:0] y;

initial begin
	$dumpfile("muxN_1_tb_a.vcd");
	$dumpvars(0, muxN_1_tb_a);
	$monitor("time=%2t a=%b b=%b s=%b y%b", $time, a, b, s, y);

	a = 32'h0;
	b = 32'h4F5C7AC2;

	s = 32'h56DE32BA;

	#5 $finish;
end

muxN_1 #(.n(n))CUT1 (.a(a), .b(b), .s(s), .y(y));

endmodule
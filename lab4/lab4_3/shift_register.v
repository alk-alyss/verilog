module shift_register (clk, in, ctrl, rst, out);
parameter n = 3;

input clk, in, ctrl, rst;
output reg out;

reg [n-1:0] d;

initial begin
	d = 0;
end

always @ (posedge clk) begin
	if(rst == 1'b1) begin
		d = 0;
		out = d[n-1];
	end
	else if(ctrl == 1'b0) begin
		d = d << 1;
		d[0] = in;
		out = d[n-1];
	end
	else if(ctrl == 1'b1) begin
		d = d >> 1;
		d[n] = in;
		out = d[0];
	end

end

endmodule
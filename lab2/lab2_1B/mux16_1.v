module mux16_1(a, s, y);

input [15:0] a; input [3:0] s;
output reg y;

always @* begin y = a[s]; end 
endmodule
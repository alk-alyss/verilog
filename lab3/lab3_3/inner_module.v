module inner_module(cin, a, b, s, cout);

input cin;
input [3:0] a, b;
output cout;
output [3:0] s;

wire [1:0] c;
wire [3:0] t, r;

cla4 U1 (.cin(1'b0), .a(a), .b(b), .s(t), .cout(c[0]));
cla4 U2 (.cin(1'b1), .a(a), .b(b), .s(r), .cout(c[1]));

mux2_1 M1 (.a(c[0]), .b(c[1]), .s(cin), .y(cout));
muxN2_1 #(.n(4)) M2 (.a(t), .b(r), .s(cin), .y(s));

endmodule
module Example_Circuit (F, G, A, B, C, D);

input	A, B, C, D;
output	F, G;
wire	w1, w2, w3;

nand	G1 (w1, A, B);
nor		G2 (w2, C, D);
xor		G3 (w3, A, C);
and		G4 (F, w1, w2);
or		G5 (G, w2, w3);

endmodule



module Example_Circuit_testbench;

reg		A, B, C, D;
wire	F, G;

Example_Circuit	CUT1 (F, G, A, B, C, D);

initial
begin
	A=0; B=0; C=0; D=0;
#5	A=0; B=0; C=0; D=1;
#5	A=0; B=0; C=1; D=0;
#5	A=0; B=0; C=1; D=1;
#5	A=0; B=1; C=0; D=0;
#5	A=0; B=1; C=0; D=1;
#5	A=0; B=1; C=1; D=0;
#5	A=0; B=1; C=1; D=1;
#5	A=1; B=0; C=0; D=0;
#5	A=1; B=0; C=0; D=1;
#5	A=1; B=0; C=1; D=0;
#5	A=1; B=0; C=1; D=1;
#5	A=1; B=1; C=0; D=0;
#5	A=1; B=1; C=0; D=1;
#5	A=1; B=1; C=1; D=0;
#5	A=1; B=1; C=1; D=1;
#5 $stop;
end

initial $monitor($time, ,"A=%b, B=%b, C=%b, D=%b, F=%b, G=%b",A, B, C, D, F, G);

endmodule
module mux8_1 (a, s, y);

input	[7:0] a;
input	[2:0] s;
output y;
wire w1, w2, w3, w4, w5, w6;

mux2_1 m1 (.a(a[7]), .b(a[6]), .s(s[0]), .y(w1));
mux2_1 m2 (.a(a[5]), .b(a[4]), .s(s[0]), .y(w2));
mux2_1 m3 (.a(a[3]), .b(a[2]), .s(s[0]), .y(w3));
mux2_1 m4 (.a(a[1]), .b(a[0]), .s(s[0]), .y(w4));
mux2_1 m5 (.a(w1), .b(w2), .s(s[1]), .y(w5));
mux2_1 m6 (.a(w3), .b(w4), .s(s[1]), .y(w6));
mux2_1 m7 (.a(w5), .b(w6), .s(s[2]), .y(y));

endmodule
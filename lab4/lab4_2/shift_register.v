module shift_register (clk, in, ctrl, rst, out);
parameter n = 3;

input clk, in, ctrl, rst;
output out;

wire [n:0] dout;
wire [n-1:0] din;

assign dout[0] = in;
genvar i;

generate
	for(i=0; i<n; i=i+1) begin
		mux2_1 M1 (
			.a(dout[i]),
			.b(dout[i+2>n?0:i+2]),
			.s(ctrl),
			.y(din[i])
		);

		d_ff D1 (
			.clk(clk),
			.d(din[i]),
			.rst(rst),
			.q(dout[i+1])
		);
	end
endgenerate

assign out = ctrl ? dout[1] : dout[n];

endmodule
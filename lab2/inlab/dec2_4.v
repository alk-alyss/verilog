module dec2_4 (w0, w1, en, y0, y1, y2, y3);

input w0, w1, en;
output reg y0, y1, y2, y3;

always @ (w0, w1, en)
	if (en==0)
		{y3, y2, y1, y0} = 4'b0000;
	else
		case ({w1,w0})
		2'b00: {y3,y2,y1,y0} = 4'b0001;
		2'b01: {y3,y2,y1,y0} = 4'b0010;
		2'b10: {y3,y2,y1,y0} = 4'b0100;
		2'b11: {y3,y2,y1,y0} = 4'b1000;
		default: {y3,y2,y1,y0} = 4'b0000;
	endcase

endmodule
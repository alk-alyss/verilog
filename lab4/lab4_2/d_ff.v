module d_ff (clk, d, rst, q);

input clk, d, rst;
output reg q;

always @ (posedge clk) begin
	q = rst==1'b1 ? 1'b0 : d;
end

endmodule
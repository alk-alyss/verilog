module mux16_1_tb;

reg	[15:0] a;
reg	[3:0] s;
output y;

mux16_1 cut1(.a(a), .s(s), .y(y));

initial begin
	$dumpfile("mux16_1_tb.vcd");
	$dumpvars(0, mux16_1_tb);
	$monitor("time=%2t a=%b s=%b y=%b", $time, a, s, y);
	
	a = 16'b0011011100111011; 
	s = 4'b0000;

	for(integer i=0; i<16; i=i+1)
		#5	s = i;

	#5 $display("");

	a = 16'b1101100110110100; 
	s = 4'b0000;

	for(integer i=0; i<16; i=i+1)
		#5	s = i;

#5 $finish;
end

endmodule
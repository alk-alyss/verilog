module 	Circuit2_structure (F, G, A, B, C, D, E);

input	A, B, C, D, E;
output	F, G;
wire	w1, w2, w3;

nor		G1 (w1, A, B);
and		G2 (w2, C, D);
nand	G3 (w3, E, C);
or		G4 (F, w1, w2, w3);
and		G5 (G, w3, F);

endmodule



module	Circuit2_dataflow (F, G, A, B, C, D, E);

input	A, B, C, D, E;
output	F, G;

assign	F = (~(A|B))|(C&D)|(~(E&C));
assign	G = F & (~(E&C));

endmodule



module Circuit2_structure_testbench;

reg		A, B, C, D, E;
wire	F, G;

Circuit2_structure	CUT1 (F, G, A, B, C, D, E);

initial
begin
	A=0; B=0; C=0; D=0; E=0;
#5	A=0; B=0; C=0; D=1; E=0; 
#5	A=0; B=0; C=1; D=0; E=0;
#5	A=0; B=0; C=1; D=1; E=0;
#5	A=0; B=1; C=0; D=0; E=0;
#5	A=0; B=1; C=0; D=1; E=0;
#5	A=0; B=1; C=1; D=0; E=0;
#5	A=0; B=1; C=1; D=1; E=0;
#5	A=1; B=0; C=0; D=0; E=0;
#5	A=1; B=0; C=0; D=1; E=0;
#5	A=1; B=0; C=1; D=0; E=0;
#5	A=1; B=0; C=1; D=1; E=0;
#5	A=1; B=1; C=0; D=0; E=0;
#5	A=1; B=1; C=0; D=1; E=0;
#5	A=1; B=1; C=1; D=0; E=0;
#5	A=1; B=1; C=1; D=1; E=0;
#5	A=0; B=0; C=0; D=0; E=1;
#5	A=0; B=0; C=0; D=1; E=1; 
#5	A=0; B=0; C=1; D=0; E=1;
#5	A=0; B=0; C=1; D=1; E=1;
#5	A=0; B=1; C=0; D=0; E=1;
#5	A=0; B=1; C=0; D=1; E=1;
#5	A=0; B=1; C=1; D=0; E=1;
#5	A=0; B=1; C=1; D=1; E=1;
#5	A=1; B=0; C=0; D=0; E=1;
#5	A=1; B=0; C=0; D=1; E=1;
#5	A=1; B=0; C=1; D=0; E=1;
#5	A=1; B=0; C=1; D=1; E=1;
#5	A=1; B=1; C=0; D=0; E=1;
#5	A=1; B=1; C=0; D=1; E=1;
#5	A=1; B=1; C=1; D=0; E=1;
#5	A=1; B=1; C=1; D=1; E=1;
#5 $stop;
end

initial $monitor($time, ,"A=%b, B=%b, C=%b, D=%b, E=%b, F=%b, G=%b", A, B, C, D, E, F, G);
endmodule



module Circuit2_dataflow_testbench;

reg		A, B, C, D, E;
wire	F, G;

Circuit2_structure	CUT1 (F, G, A, B, C, D, E);

initial
begin
	A=0; B=0; C=0; D=0; E=0;
#5	A=0; B=0; C=0; D=1; E=0; 
#5	A=0; B=0; C=1; D=0; E=0;
#5	A=0; B=0; C=1; D=1; E=0;
#5	A=0; B=1; C=0; D=0; E=0;
#5	A=0; B=1; C=0; D=1; E=0;
#5	A=0; B=1; C=1; D=0; E=0;
#5	A=0; B=1; C=1; D=1; E=0;
#5	A=1; B=0; C=0; D=0; E=0;
#5	A=1; B=0; C=0; D=1; E=0;
#5	A=1; B=0; C=1; D=0; E=0;
#5	A=1; B=0; C=1; D=1; E=0;
#5	A=1; B=1; C=0; D=0; E=0;
#5	A=1; B=1; C=0; D=1; E=0;
#5	A=1; B=1; C=1; D=0; E=0;
#5	A=1; B=1; C=1; D=1; E=0;
#5	A=0; B=0; C=0; D=0; E=1;
#5	A=0; B=0; C=0; D=1; E=1; 
#5	A=0; B=0; C=1; D=0; E=1;
#5	A=0; B=0; C=1; D=1; E=1;
#5	A=0; B=1; C=0; D=0; E=1;
#5	A=0; B=1; C=0; D=1; E=1;
#5	A=0; B=1; C=1; D=0; E=1;
#5	A=0; B=1; C=1; D=1; E=1;
#5	A=1; B=0; C=0; D=0; E=1;
#5	A=1; B=0; C=0; D=1; E=1;
#5	A=1; B=0; C=1; D=0; E=1;
#5	A=1; B=0; C=1; D=1; E=1;
#5	A=1; B=1; C=0; D=0; E=1;
#5	A=1; B=1; C=0; D=1; E=1;
#5	A=1; B=1; C=1; D=0; E=1;
#5	A=1; B=1; C=1; D=1; E=1;
#5 $stop;
end

initial $monitor($time, ,"A=%b, B=%b, C=%b, D=%b, E=%b, F=%b, G=%b",A, B, C, D, E, F, G);

endmodule
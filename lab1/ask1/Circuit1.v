module	Circuit1_structure (F, A, B, C, D);

input 	A, B, C, D;
output 	F;
wire 	Ainv, w1, w2, w3, w4, w5;

not 	Inv	(Ainv, A);
and 	G1 	(w1, A, B);
xnor	G2 	(w2, C, D);
nand	G3 	(w3, Ainv, C);
nor 	G4	(w4, w1, w2, w3);
nand	G5	(w5, w1, w2);
xor		G6	(F, w4, w5);

endmodule



module	Circuit1_dataflow (F, A, B, C, D);

input	A, B, C, D;
output	F;

assign	F = (~((A&B)|(C~^D)|(~((~A)&C)))) ^ (~((A&B)&(C~^D)));

endmodule



module Circuit1_structure_testbench;

reg		A, B, C, D;
wire	F;

Circuit1_structure	CUT1 (F, A, B, C, D);

initial
begin
	A=0; B=0; C=0; D=0;
#5	A=0; B=0; C=0; D=1;
#5	A=0; B=0; C=1; D=0;
#5	A=0; B=0; C=1; D=1;
#5	A=0; B=1; C=0; D=0;
#5	A=0; B=1; C=0; D=1;
#5	A=0; B=1; C=1; D=0;
#5	A=0; B=1; C=1; D=1;
#5	A=1; B=0; C=0; D=0;
#5	A=1; B=0; C=0; D=1;
#5	A=1; B=0; C=1; D=0;
#5	A=1; B=0; C=1; D=1;
#5	A=1; B=1; C=0; D=0;
#5	A=1; B=1; C=0; D=1;
#5	A=1; B=1; C=1; D=0;
#5	A=1; B=1; C=1; D=1;
#5 $stop;
end

initial $monitor($time, ,"A=%b, B=%b, C=%b, D=%b, F=%b",A, B, C, D, F);
endmodule



module Circuit1_dataflow_testbench;

reg		A, B, C, D;
wire	F;

Circuit1_dataflow	CUT1 (F, A, B, C, D);

initial
begin
	A=0; B=0; C=0; D=0;
#5	A=0; B=0; C=0; D=1;
#5	A=0; B=0; C=1; D=0;
#5	A=0; B=0; C=1; D=1;
#5	A=0; B=1; C=0; D=0;
#5	A=0; B=1; C=0; D=1;
#5	A=0; B=1; C=1; D=0;
#5	A=0; B=1; C=1; D=1;
#5	A=1; B=0; C=0; D=0;
#5	A=1; B=0; C=0; D=1;
#5	A=1; B=0; C=1; D=0;
#5	A=1; B=0; C=1; D=1;
#5	A=1; B=1; C=0; D=0;
#5	A=1; B=1; C=0; D=1;
#5	A=1; B=1; C=1; D=0;
#5	A=1; B=1; C=1; D=1;
#5 $stop;
end

initial $monitor($time, ,"A=%b, B=%b, C=%b, D=%b, F=%b",A, B, C, D, F);

endmodule
module select8_tb;

reg [7:0] a, b;
reg cin;
output [7:0] s;
output cout;

initial begin
	$dumpfile("select8_tb.vcd");
	$dumpvars(0, select8_tb);
	$monitor("time=%2t cin=%b a=%b b=%b s=%b cout=%b", $time, cin, a, b, s, cout);

	cin = 1'b0; a = 8'hFF; b = 8'h01;
	#5 cin = 1'b0; a = 8'hFF; b = 8'h00;
	#5 cin = 1'b0; a = 8'h05; b = 8'h01;
	#5 cin = 1'b0; a = 8'h01; b = 8'h21;
	#5 cin = 1'b0; a = 8'hAA; b = 8'h55;
	#5 cin = 1'b1; a = 8'hAA; b = 8'h55;
	#5 cin = 1'b0; a = 8'hFD; b = 8'h00;
	#5 cin = 1'b1; a = 8'h01; b = 8'h01;

	#5 $finish;
end

select8 CUT1 (.cin(cin), .a(a), .b(b), .s(s), .cout(cout));

endmodule
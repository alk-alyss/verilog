module enc4_2 (d, y, zero);

input [3:0] d;
output reg [1:0] y;
output reg zero;

always @* begin
	zero = 0;
	if(d == 0) begin
		y = 2'b00;
		zero = 1'b1;
	end
	else begin
		for(integer i=0; i<=7; i=i+1) begin
			if(d[i] == 1)
				y = i;
		end
	end
end

endmodule
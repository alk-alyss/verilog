module mux8_1_tb;

reg	[7:0] a;
reg	[2:0] s;
output y;

mux8_1 cut1(.a(a), .s(s), .y(y));

initial begin
	$dumpfile("mux8_1_tb.vcd");
	$dumpvars(0, mux8_1_tb);
	$monitor("time=%2t a=%b s=%b y=%b", $time, a, s, y);
	
	a = 7'b1011001; 
	s = 3'b000;

	for(integer i=0; i<8; i=i+1)
		#5	s = i;

	#5 $display("");
	
	a = 7'b0010011; 
	s = 3'b000;

	for(integer i=0; i<8; i=i+1)
		#5	s = i;

#5 $finish;
end

endmodule
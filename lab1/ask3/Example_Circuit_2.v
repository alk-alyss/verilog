module	Example_Circuit2 (F, A, B, C);

input	A, B, C;
output	F;
wire	w1, w2, w3, invB, invC;

not		Binv (invB, B);
not		Cinv (invC, C);
and		G1 (w1, invB, C);
and		G2 (w2, invC, B);
and		G3 (w3, A, B);
or		G4 (F, w1, w2, w3);

endmodule

module	Example_Circuit2_testbench;

reg		A, B, C;
wire	F;

Example_Circuit2	CUT1 (F, A, B, C);

initial
begin
	A=0; B=0; C=0;
#10	A=1; B=0; C=0;
#10	A=0; B=1; C=0;
#10	A=1; B=1; C=0;
#10	A=0; B=0; C=1;
#10	A=1; B=0; C=1;
#10	A=0; B=1; C=1;
#10	A=1; B=1; C=1;
#10 $stop;
end

initial $monitor($time, ,"A=%b, B=%b, C=%b, F=%b",A, B, C, F);

endmodule
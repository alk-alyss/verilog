module filter_tb;
parameter CLOCK = 5;

reg in, clk, reset;
output out;

initial begin
	$dumpfile("filter_tb.vcd");
	$dumpvars(0, filter_tb);
	$monitor("time=%2t clk=%b reset=%b in=%b out=%b", $time, clk, reset, in, out);

	clk = 1'b0;
	reset = 1'b0;
	in = 1'b0;

	#10 in = 1'b1;
	#30 reset = 1'b1;
	#10 reset = 1'b0;
	#30 in = 1'b0;

	#10 in = 1'b1;
	#30 in = 1'b0;
	#20 in = 1'b1;
	#20 in = 1'b0;
	#10 in = 1'b1;
	#10 in = 1'b0;

	#30 $finish;

end

always begin
	#CLOCK clk = ~clk;
end

filter CUT1 (
	.in(in),
	.clk(clk),
	.reset(reset),
	.out(out)
);

endmodule
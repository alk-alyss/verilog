module enc8_3(d, y);

input [7:0] d;
output reg [2:0] y;

always @* begin

	for(integer i=0; i<=7; i=i+1) begin
		if(d[i] == 1)
			y = i;
	end

end

endmodule
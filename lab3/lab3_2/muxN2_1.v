module muxN2_1(a, b, s, y);
parameter n = 4;


input [n-1:0] a, b;
input s;
output [n-1:0] y;

genvar i;

generate
	for(i=0; i<n; i=i+1) begin
		mux2_1 mux(.a(a[i]), .b(b[i]), .s(s), .y(y[i]));
	end
endgenerate

endmodule
module dec4_16v2 (
	x0, x1, x2, x3, 
	en, 
	z0, z1, z2, z3, z4, z5, z6, z7, z8, z9, z10, z11, z12, z13, z14, z15
);

input x0, x1, x2, x3, en;
output z0, z1, z2, z3, z4, z5, z6, z7, z8, z9, z10, z11, z12, z13, z14, z15;
wire w1, w2, w3, w4;

dec2_4 dec0 (x2, x3, en, w1, w2, w3, w4);
dec2_4 dec1 (x0, x1, w1, z0, z1, z2, z3);
dec2_4 dec2 (x0, x1, w2, z4, z5, z6, z7);
dec2_4 dec3 (x0, x1, w3, z8, z9, z10, z11);
dec2_4 dec4 (x0, x1, w4, z12, z13, z14, z15);

endmodule

module dec4_16v2tb;

reg x0, x1, x2, x3, en;
wire z0, z1, z2, z3, z4, z5, z6, z7, z8, z9, z10, z11, z12, z13, z14, z15;

dec4_16v2 CUT1 (x0, x1, x2, x3, en, z0, z1, z2, z3, z4, z5, z6, z7, z8, z9, z10, z11, z12, z13, z14, z15);

initial
begin
	x0=1; x1=0; x2=1; x3=0; en=0;
#5	x0=1; x1=1; x2=1; x3=1; en=1;
#5	x0=0; x1=1; x2=0; x3=1; en=1;
#5	x0=0; x1=0; x2=1; x3=0; en=1;
#5	x0=1; x1=0; x2=0; x3=1; en=1;
#5	$stop;
end

initial $monitor($time, ,"en=%b, x0=%b, x1=%b, x2=%b, x3=%b, z0=%b, z1=%b, z2=%b, z3=%b, z4=%b, z5=%b, z6=%b, z7=%b, z8=%b, z9=%b, z10=%b, z11=%b, z12=%b, z13=%b, z14=%b, z15=%b", en, x0, x1, x2, x3, z0, z1, z2, z3, z4, z5, z6, z7, z8, z9, z10, z11, z12, z13, z14, z15);

endmodule
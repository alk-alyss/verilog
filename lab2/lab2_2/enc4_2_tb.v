module enc4_2_tb;

reg [3:0] d;
output [1:0] y;
output zero;

initial	begin
	$dumpfile("enc4_2_tb.vcd");
	$dumpvars(0, enc4_2_tb);
	$monitor("time=%2t d=%b y=%b zero=%b", $time, d, y, zero);

	d = 1'h0;

	for(integer i=0; i<16; i=i+1)
		#5 d = i;

	#5 $finish;
end

enc4_2 cut1 (.d(d), .y(y), .zero(zero));

endmodule

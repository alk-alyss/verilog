module muxN_1_tb_b;
parameter n = 16;

reg [n-1:0] a, b, s;
output [n-1:0] y;

initial begin
	$dumpfile("muxN_1_tb_b.vcd");
	$dumpvars(0, muxN_1_tb_b);
	$monitor("time=%2t a=%b b=%b s=%b y%b", $time, a, b, s, y);

	a = 16'h0;
	b = 16'h7AC2;

	s = 16'h32BA;

	#5 $finish;
end

muxN_1 #(.n(n))CUT1 (.a(a), .b(b), .s(s), .y(y));

endmodule